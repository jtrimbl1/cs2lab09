package edu.westga.cs1302.exceptions.controllers;

import java.util.ArrayList;

import edu.westga.cs1302.exceptions.model.Dog;
import edu.westga.cs1302.exceptions.model.ShowDog;
import edu.westga.cs1302.exceptions.model.WorkDog;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class defines the controller functions for the exceptions application
 * 
 * @author jeremy.trimble
 * 
 * @version 7/9/2018
 *
 */
public class DogController {
	private ArrayList<Dog> dogs;

	/**
	 * Creates the controller object
	 * 
	 * @precondition: none
	 * @postcondition instance variables are initialized
	 * 
	 */
	public DogController() {
		this.dogs = new ArrayList<Dog>();
	}

	/**
	 * Creates a new show dog
	 * 
	 * @precondition: none
	 * 
	 * @param name
	 *            The name of the dog
	 * @param age
	 *            The age of the dog
	 * @param registrationNumber
	 *            The registration number of the dog
	 */
	public void createShowDog(String name, String age, String registrationNumber) {
		try {
			int intAge = Integer.parseInt(age);
			int intRegistration = Integer.parseInt(registrationNumber);
			Dog newDog = new ShowDog(name, intAge, intRegistration);
			this.dogs.add(newDog);
		} catch (IllegalArgumentException iAE) {
			Alert alert = new Alert(AlertType.ERROR,
					"Age and Registration Number must be greater than zero and written as a number");
			alert.showAndWait();

		}
	}

	/**
	 * Creates a new WorkingDog
	 * 
	 * @param name
	 *            The name of the dog
	 * @param age
	 *            The age of the dog
	 * @param maximumWorkHours
	 *            The maximum number of working hours for the dog
	 */
	public void createWorkingDog(String name, String age, String maximumWorkHours) {

		try {
			int intAge = Integer.parseInt(age);
			int intMaximumWorkHours = Integer.parseInt(maximumWorkHours);
			Dog newDog = new WorkDog(name, intAge, intMaximumWorkHours);
			this.dogs.add(newDog);
		} catch (IllegalArgumentException iAE) {
			Alert alert = new Alert(AlertType.ERROR,
					"Age and maximum working hours must be greater than zero and written as a number");
			alert.showAndWait();
		}
	}

	/**
	 * This method will build a String representation of the dogs in the collection
	 * with one dog included per line
	 * 
	 * @return A String holding a description of each dog, with one dog per line
	 */
	public String getDescription() {
		String returnString = "";

		for (Dog current : this.dogs) {
			returnString += current.toString() + "\n";
		}
		return returnString;
	}

}
