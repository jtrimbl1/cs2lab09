package edu.westga.cs1302.exceptions.model;

/**
 * Creates Dog objects
 * 
 * 
 * @author jeremy.trimble
 * @version 7/9/2018
 */
public class Dog {

	private String name;
	private int age;

	/**
	 * Constructor for a dog object
	 * 
	 * @precondition: name != null age > 0
	 * @postcondition: new dog object is created
	 * 
	 * @param name
	 *            The dog's name
	 * @param age
	 *            The dog's age
	 */
	public Dog(String name, int age) {
		if (name == null) {
			throw new IllegalArgumentException("Dog name must not be null");
		}

		if (age <= 0) {
			throw new IllegalArgumentException("Age must be greater than 0");
		}

		this.name = name;
		this.age = age;
	}

	/**
	 * Overrides the toString method
	 * 
	 * @precondtion: none
	 * @return String describing the dog object
	 * 
	 */
	public String toString() {
		return "Dog " + this.name + " is " + this.age + " years old.";
	}

	/**
	 * Returns the dog's age
	 * 
	 * @precondition: none
	 * 
	 * @return int of the dog's age
	 * 
	 */
	public int getAge() {
		return this.age;
	}

	/**
	 * Returns the name of the dog
	 * 
	 * @precondition: none
	 * 
	 * @return String of the dog's name
	 * 
	 */
	public String getName() {
		return this.name;
	}
}
