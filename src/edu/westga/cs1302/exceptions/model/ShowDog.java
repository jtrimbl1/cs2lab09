package edu.westga.cs1302.exceptions.model;

/**
 * Extends dog class by including the AKC registration number
 * 
 * @author jeremy.trimble
 * @version 7/9/2018
 */
public class ShowDog extends Dog {

	private int akcNumber;

	/**
	 * Creates a ShowDog object with the specified name age and registration number
	 * 
	 * @precondition: akcNumber > 0
	 * 
	 * @postcondition: new Show Dog object is created
	 * 
	 * @param name
	 *            The name of the dog
	 * @param age
	 *            The age of the dog
	 * 
	 * @param akcNumber
	 *            The AKC registration number for the dog
	 */
	public ShowDog(String name, int age, int akcNumber) {
		super(name, age);
		if (akcNumber <= 0) {
			throw new IllegalArgumentException("AKC Number must be greater than 0.");
		}

		this.akcNumber = akcNumber;
	}

	/**
	 * Overrides the toSTring Method
	 * 
	 * @precondition: none
	 * 
	 * @return String that describes the ShowDog object
	 * 
	 */
	public String toString() {
		return super.getName() + " is a " + super.getAge() + " year old show dog with an AKC Number of "
				+ this.akcNumber + ".";
	}

}
