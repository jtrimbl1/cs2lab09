package edu.westga.cs1302.exceptions.model;

/**
 * Extends Dog class by including the maximum number of working hours
 * 
 * @author jeremy.trimble
 * @version 7/9/2018
 * 
 */
public class WorkDog extends Dog {

	private int maxWorkHours;

	/**
	 * Creates a Working Dog object
	 * 
	 * @precondition: maxWorkHours >= 0
	 * 
	 * @postcondition: new working dog object is created
	 * 
	 * @param name
	 *            The dog's name
	 * @param age
	 *            The dog's age
	 * @param maxWorkHours
	 *            The maximum working hours for the dog
	 */
	public WorkDog(String name, int age, int maxWorkHours) {
		super(name, age);

		if (maxWorkHours < 0) {
			throw new IllegalArgumentException("Max working hours must be 0 or greater.");
		}

		this.maxWorkHours = maxWorkHours;
	}

	/**
	 * Overrides toString method
	 * 
	 * @precondition: none
	 * 
	 * @return String describing the working dog object
	 */
	public String toString() {
		return super.getName() + " is a " + super.getAge() + " year old working dog with a maximum working hours of "
				+ this.maxWorkHours + ".";
	}

}
