package edu.westga.cs1302.exceptions.model.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.Dog;

/**
 * Test the creation of a dog object and it's getters
 * 
 * @author jeremy.trimble
 * @version 7/9/2018
 */
public class TestDog {

	/**
	 * Test the constructor
	 * 
	 * @precondition: none
	 * 
	 */
	@Test
	public void testCreateDog() {
		Dog newDog = new Dog("Jerry", 5);

		assertEquals("Dog Jerry is 5 years old.", newDog.toString());
	}

	/**
	 * Test the getName method
	 *
	 * @precondition: none
	 *
	 */
	@Test
	public void testGetName() {
		Dog newDog = new Dog("Jerry", 5);

		assertEquals("Jerry", newDog.getName());
	}

	/**
	 * Test the getAge method
	 *
	 * @precondition: none
	 *
	 */
	@Test
	public void testGetAge() {
		Dog newDog = new Dog("Jerry", 5);

		assertEquals(5, newDog.getAge());
	}

}
