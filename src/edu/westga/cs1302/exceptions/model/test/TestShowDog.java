package edu.westga.cs1302.exceptions.model.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.ShowDog;

/**
 * Test the creation of a Show Dog
 *
 * @author jeremy.trimble
 * @version 7/9/2018
 */
public class TestShowDog {

	/**
	 * Test the constructor of the showdog
	 * 
	 * @precondition: none
	 * 
	 */
	@Test
	public void testCreateShowDog() {
		ShowDog newDog = new ShowDog("Tim", 9, 345);

		assertEquals("Tim is a 9 year old show dog with an AKC Number of 345.", newDog.toString());
	}

}
