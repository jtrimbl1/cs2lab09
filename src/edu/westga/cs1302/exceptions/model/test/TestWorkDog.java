package edu.westga.cs1302.exceptions.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.WorkDog;

/**
 * Test creating a Work dog.s
 * 
 * @author jeremy.trimble
 * @version 7/9/2018
 */
class TestWorkDog {

	/**
	 * Test the creation of a Work Dog
	 * 
	 * @precondition: none
	 * 
	 */
	@Test
	void testCreateWorkDog() {
		WorkDog newWorker = new WorkDog("Bob", 6, 6);

		assertEquals("Bob is a 6 year old working dog with a maximum working hours of 6.", newWorker.toString());
	}

}
